

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printWelcomeMessages() {
	let welcomeName = prompt("What is your Name?");
	let welcomeAge = prompt("How Old are you?");
	let welcomeAddress = prompt("Where do you live?");

	console.log("Hello, " + welcomeName);
	console.log("You are" + welcomeAge + " years old.");
	console.log("You live in " + welcomeAddress);

}

printWelcomeMessages();


 function sampleAlert() {
 	alert("Thank you for your Input");
 }

 sampleAlert();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/


	//second function here:

	function printName(){
	console.log("1. Eraserheads");
	console.log("2. Parokya ni Edgar");
	console.log("3. Kamikazee");
	console.log("4. Urbandub");
	console.log("5. Silent Sanctuary");

};

printName();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function showNames() {
	// Function scope variables:
	const functionConst = "1. The Godfather";
	let functionLet = "Rotten Tomatoes Rating: 97%";

	const functionConst1 = "2. The Godfather, Part II";
	let functionLet1 = "Rotten Tomatoes Rating: 96%";

	const functionConst2 = "3. Shawshank Redemption";
	let functionLet2 = "Rotten Tomatoes Rating: 91%";

	const functionConst3 = "4. To Kill A Mockingbird";
	let functionLet3 = "Rotten Tomatoes Rating: 93%";

	const functionConst4 = "5. Psycho";
	let functionLet4 = "Rotten Tomatoes Rating: 96%";

	console.log(functionConst);
	console.log(functionLet);

	console.log(functionConst1);
	console.log(functionLet1);

	console.log(functionConst2);
	console.log(functionLet2);

	console.log(functionConst3);
	console.log(functionLet3);

	console.log(functionConst4);
	console.log(functionLet4);
}

showNames();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();